#region Using declarations
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Data;
using NinjaTrader.Gui.Chart;
using NadexDBLib;
using TransportModel;
using System.Collections;
using System.Collections.Generic;
#endregion

// This namespace holds all indicators and is required. Do not change it.
namespace NinjaTrader.Indicator
{

    [Description("Enter the description of your new custom indicator here")]
    public class MyPriceUpdateIndicator : Indicator
    {
        #region Variables
            private int myInput0 = 1; // Default setting for MyInput0
			public static int counter = 0;
        #endregion
		
		protected override void OnStartUp()
		{
			Print("OnStartUp : MyPriceUpdateIndicator");
			Print(Instrument.FullName.ToString());
			
			Scheduler s = Scheduler.Instance;
			s.Name = Instrument.FullName.ToString();
			s.WorkTask.Add(new WorkTaskUpdateInstrument());
			
            s.Timer.Enabled = true;
			//every 1 min     //milli  //sec  //min
            s.Timer.Interval = (1000) * (60) * (1);
            s.Timer.Start();
            s.PropertyChanged += s_PropertyChanged; 
		}

		public void s_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            HandlePropertyChange(sender, e);
			counter++;
        }
		
 		private void HandlePropertyChange(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if ((e.PropertyName == "ResultList") && (sender.GetType() == typeof(ArrayList)))
            {
                ArrayList lists = (ArrayList)sender;

                foreach (var item in lists)
                {
					/*UPDATE PRICE*/
					Scheduler s = Scheduler.Instance;
                    if (item.GetType() == typeof(List<string>))
                    {
                        List<string> priceStatus = (List<string>)(item);
						Print(string.Format("count: {0}\t Price: {1} \t {2}",counter ,s.Price, priceStatus[0].ToString()));
                    }
					
                }
            }
        }
		
        protected override void Initialize()
        {
            Add(new Plot(Color.FromKnownColor(KnownColor.Orange), PlotStyle.Line, "Plot0"));
            Overlay				= false;
        }


        protected override void OnBarUpdate()
        {
			Scheduler s = Scheduler.Instance;
			s.Price = Close[0];
            Plot0.Set(Close[0]);
			Print(string.Format("\t\tcurrent OpenPrice is : {0}",Open[0]));
			Print(string.Format("\t\tcurrent ClosePrice is : {0}",Close[0]));
        }

        #region Properties
        [Browsable(false)]	// this line prevents the data series from being displayed in the indicator properties dialog, do not remove
        [XmlIgnore()]		// this line ensures that the indicator can be saved/recovered as part of a chart template, do not remove
        public DataSeries Plot0
        {
            get { return Values[0]; }
        }

        [Description("")]
        [GridCategory("Parameters")]
        public int MyInput0
        {
            get { return myInput0; }
            set { myInput0 = Math.Max(1, value); }
        }
        #endregion
    }
}

#region NinjaScript generated code. Neither change nor remove.
// This namespace holds all indicators and is required. Do not change it.
namespace NinjaTrader.Indicator
{
    public partial class Indicator : IndicatorBase
    {
        private MyPriceUpdateIndicator[] cacheMyPriceUpdateIndicator = null;

        private static MyPriceUpdateIndicator checkMyPriceUpdateIndicator = new MyPriceUpdateIndicator();

        /// <summary>
        /// Enter the description of your new custom indicator here
        /// </summary>
        /// <returns></returns>
        public MyPriceUpdateIndicator MyPriceUpdateIndicator(int myInput0)
        {
            return MyPriceUpdateIndicator(Input, myInput0);
        }

        /// <summary>
        /// Enter the description of your new custom indicator here
        /// </summary>
        /// <returns></returns>
        public MyPriceUpdateIndicator MyPriceUpdateIndicator(Data.IDataSeries input, int myInput0)
        {
            if (cacheMyPriceUpdateIndicator != null)
                for (int idx = 0; idx < cacheMyPriceUpdateIndicator.Length; idx++)
                    if (cacheMyPriceUpdateIndicator[idx].MyInput0 == myInput0 && cacheMyPriceUpdateIndicator[idx].EqualsInput(input))
                        return cacheMyPriceUpdateIndicator[idx];

            lock (checkMyPriceUpdateIndicator)
            {
                checkMyPriceUpdateIndicator.MyInput0 = myInput0;
                myInput0 = checkMyPriceUpdateIndicator.MyInput0;

                if (cacheMyPriceUpdateIndicator != null)
                    for (int idx = 0; idx < cacheMyPriceUpdateIndicator.Length; idx++)
                        if (cacheMyPriceUpdateIndicator[idx].MyInput0 == myInput0 && cacheMyPriceUpdateIndicator[idx].EqualsInput(input))
                            return cacheMyPriceUpdateIndicator[idx];

                MyPriceUpdateIndicator indicator = new MyPriceUpdateIndicator();
                indicator.BarsRequired = BarsRequired;
                indicator.CalculateOnBarClose = CalculateOnBarClose;
#if NT7
                indicator.ForceMaximumBarsLookBack256 = ForceMaximumBarsLookBack256;
                indicator.MaximumBarsLookBack = MaximumBarsLookBack;
#endif
                indicator.Input = input;
                indicator.MyInput0 = myInput0;
                Indicators.Add(indicator);
                indicator.SetUp();

                MyPriceUpdateIndicator[] tmp = new MyPriceUpdateIndicator[cacheMyPriceUpdateIndicator == null ? 1 : cacheMyPriceUpdateIndicator.Length + 1];
                if (cacheMyPriceUpdateIndicator != null)
                    cacheMyPriceUpdateIndicator.CopyTo(tmp, 0);
                tmp[tmp.Length - 1] = indicator;
                cacheMyPriceUpdateIndicator = tmp;
                return indicator;
            }
        }
    }
}

// This namespace holds all market analyzer column definitions and is required. Do not change it.
namespace NinjaTrader.MarketAnalyzer
{
    public partial class Column : ColumnBase
    {
        /// <summary>
        /// Enter the description of your new custom indicator here
        /// </summary>
        /// <returns></returns>
        [Gui.Design.WizardCondition("Indicator")]
        public Indicator.MyPriceUpdateIndicator MyPriceUpdateIndicator(int myInput0)
        {
            return _indicator.MyPriceUpdateIndicator(Input, myInput0);
        }

        /// <summary>
        /// Enter the description of your new custom indicator here
        /// </summary>
        /// <returns></returns>
        public Indicator.MyPriceUpdateIndicator MyPriceUpdateIndicator(Data.IDataSeries input, int myInput0)
        {
            return _indicator.MyPriceUpdateIndicator(input, myInput0);
        }
    }
}

// This namespace holds all strategies and is required. Do not change it.
namespace NinjaTrader.Strategy
{
    public partial class Strategy : StrategyBase
    {
        /// <summary>
        /// Enter the description of your new custom indicator here
        /// </summary>
        /// <returns></returns>
        [Gui.Design.WizardCondition("Indicator")]
        public Indicator.MyPriceUpdateIndicator MyPriceUpdateIndicator(int myInput0)
        {
            return _indicator.MyPriceUpdateIndicator(Input, myInput0);
        }

        /// <summary>
        /// Enter the description of your new custom indicator here
        /// </summary>
        /// <returns></returns>
        public Indicator.MyPriceUpdateIndicator MyPriceUpdateIndicator(Data.IDataSeries input, int myInput0)
        {
            if (InInitialize && input == null)
                throw new ArgumentException("You only can access an indicator with the default input/bar series from within the 'Initialize()' method");

            return _indicator.MyPriceUpdateIndicator(input, myInput0);
        }
    }
}
#endregion
