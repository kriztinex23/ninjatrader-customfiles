#region Using declarations
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Data;
using NinjaTrader.Gui.Chart;
using System.Collections;
using NadexDBLib;
using TransportModel;

using System.Collections.Generic;
#endregion

namespace NinjaTrader.Indicator
{

    [Description("Enter the description of your new custom indicator here")]
    public class MyCustomIndicator : Indicator
    {
        #region Variables
            private int myInput0 = 1; // Default setting for MyInput0
			public static int counter = 0;
        #endregion
		
		private bool isOkToTrade = false;
		
		protected override void OnStartUp()
		{

			Print("OnStartUp : MyCustomIndicator");
			Print(Instrument.FullName.ToString());
			Scheduler s = Scheduler.Instance;
			//s.AssemblyName = "~\\Custom\\NadexDBLib.dll";
			s.Name = Instrument.FullName.ToString().ToUpper();
			s.WorkTask.Add(new WorkTaskGetAllNews());
            s.WorkTask.Add(new WorkTaskGetCurrentNews());
			
            Print("Instantiated");
            s.Timer.Enabled = true;
			//every 1 min     //milli  //sec  //min
            s.Timer.Interval = (1000) * (5) * (1);
            s.Timer.Start();
            s.PropertyChanged += s_PropertyChanged; 
		}
		
		public void s_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            HandlePropertyChange(sender, e);
			Print(string.Format("count: {0}\tIsOkToTrade: {1}",counter ,IsOkToTrade));
			counter++;
        }
		
 		private void HandlePropertyChange(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if ((e.PropertyName == "ResultList") && (sender.GetType() == typeof(ArrayList)))
            {
                ArrayList lists = (ArrayList)sender;

                foreach (var item in lists)
                {
					/*CURRENT NEWS*/
                    if (item.GetType() == typeof(NewsInfo))
                    {
                        NewsInfo news = (NewsInfo)(item);
						isOkToTrade = (news.newsId != null);	
                        Print(string.Format("newsId : {0}", news.newsId));
					}
					
					/*GET ALL NEWS*/
					#region GET ALL NEWS
//                    if (item.GetType() == typeof(List<NewsInfo>))
//                    {
//                        List<NewsInfo> list = (List<NewsInfo>)(item);
//                        foreach (NewsInfo n in list)
//                        {
//                            Print(string.Format("News   : {0}\t{1}", n.events, n.importance));
//                        }
//                    }
					#endregion
					
					/*UPDATE PRICE*/
					#region UPDATE PRICE
//                    if (item.GetType() == typeof(List<string>))
//                    {
//                        List<string> priceStatus = (List<string>)(item);
//                        Print(string.Format("price status : \t{0}", priceStatus.FirstOrDefault().ToString()));
//                    }
					#endregion
					
                }
            }
        }
		
		
        protected override void Initialize()
        {
            Add(new Plot(Color.FromKnownColor(KnownColor.Orange), PlotStyle.Line, "Plot0"));
            Overlay				= false;
        }

        protected override void OnBarUpdate()
        {
            Plot0.Set(Close[0]);
        }

        #region Properties
        [Browsable(false)]	// this line prevents the data series from being displayed in the indicator properties dialog, do not remove
        [XmlIgnore()]		// this line ensures that the indicator can be saved/recovered as part of a chart template, do not remove
        public DataSeries Plot0
        {
            get { return Values[0]; }
        }

        [Description("")]
        [GridCategory("Parameters")]
        public int MyInput0
        {
            get { return myInput0; }
            set { myInput0 = Math.Max(1, value); }
        }
		
        public bool IsOkToTrade
        {
            get 
			{ 
				Update();
				return isOkToTrade; 
			}
            //set { isOkToTrade = value; }
        }
		
        public BoolSeries MyBoolean{get;set;}
		

        #endregion
    }
}


#region NinjaScript generated code. Neither change nor remove.
// This namespace holds all indicators and is required. Do not change it.
namespace NinjaTrader.Indicator
{
    public partial class Indicator : IndicatorBase
    {
        private MyCustomIndicator[] cacheMyCustomIndicator = null;

        private static MyCustomIndicator checkMyCustomIndicator = new MyCustomIndicator();

        /// <summary>
        /// Enter the description of your new custom indicator here
        /// </summary>
        /// <returns></returns>
        public MyCustomIndicator MyCustomIndicator(int myInput0)
        {
            return MyCustomIndicator(Input, myInput0);
        }

        /// <summary>
        /// Enter the description of your new custom indicator here
        /// </summary>
        /// <returns></returns>
        public MyCustomIndicator MyCustomIndicator(Data.IDataSeries input, int myInput0)
        {
            if (cacheMyCustomIndicator != null)
                for (int idx = 0; idx < cacheMyCustomIndicator.Length; idx++)
                    if (cacheMyCustomIndicator[idx].MyInput0 == myInput0 && cacheMyCustomIndicator[idx].EqualsInput(input))
                        return cacheMyCustomIndicator[idx];

            lock (checkMyCustomIndicator)
            {
                checkMyCustomIndicator.MyInput0 = myInput0;
                myInput0 = checkMyCustomIndicator.MyInput0;

                if (cacheMyCustomIndicator != null)
                    for (int idx = 0; idx < cacheMyCustomIndicator.Length; idx++)
                        if (cacheMyCustomIndicator[idx].MyInput0 == myInput0 && cacheMyCustomIndicator[idx].EqualsInput(input))
                            return cacheMyCustomIndicator[idx];

                MyCustomIndicator indicator = new MyCustomIndicator();
                indicator.BarsRequired = BarsRequired;
                indicator.CalculateOnBarClose = CalculateOnBarClose;
#if NT7
                indicator.ForceMaximumBarsLookBack256 = ForceMaximumBarsLookBack256;
                indicator.MaximumBarsLookBack = MaximumBarsLookBack;
#endif
                indicator.Input = input;
                indicator.MyInput0 = myInput0;
                Indicators.Add(indicator);
                indicator.SetUp();

                MyCustomIndicator[] tmp = new MyCustomIndicator[cacheMyCustomIndicator == null ? 1 : cacheMyCustomIndicator.Length + 1];
                if (cacheMyCustomIndicator != null)
                    cacheMyCustomIndicator.CopyTo(tmp, 0);
                tmp[tmp.Length - 1] = indicator;
                cacheMyCustomIndicator = tmp;
                return indicator;
            }
        }
    }
}

// This namespace holds all market analyzer column definitions and is required. Do not change it.
namespace NinjaTrader.MarketAnalyzer
{
    public partial class Column : ColumnBase
    {
        /// <summary>
        /// Enter the description of your new custom indicator here
        /// </summary>
        /// <returns></returns>
        [Gui.Design.WizardCondition("Indicator")]
        public Indicator.MyCustomIndicator MyCustomIndicator(int myInput0)
        {
            return _indicator.MyCustomIndicator(Input, myInput0);
        }

        /// <summary>
        /// Enter the description of your new custom indicator here
        /// </summary>
        /// <returns></returns>
        public Indicator.MyCustomIndicator MyCustomIndicator(Data.IDataSeries input, int myInput0)
        {
            return _indicator.MyCustomIndicator(input, myInput0);
        }
    }
}

// This namespace holds all strategies and is required. Do not change it.
namespace NinjaTrader.Strategy
{
    public partial class Strategy : StrategyBase
    {
        /// <summary>
        /// Enter the description of your new custom indicator here
        /// </summary>
        /// <returns></returns>
        [Gui.Design.WizardCondition("Indicator")]
        public Indicator.MyCustomIndicator MyCustomIndicator(int myInput0)
        {
            return _indicator.MyCustomIndicator(Input, myInput0);
        }

        /// <summary>
        /// Enter the description of your new custom indicator here
        /// </summary>
        /// <returns></returns>
        public Indicator.MyCustomIndicator MyCustomIndicator(Data.IDataSeries input, int myInput0)
        {
            if (InInitialize && input == null)
                throw new ArgumentException("You only can access an indicator with the default input/bar series from within the 'Initialize()' method");

            return _indicator.MyCustomIndicator(input, myInput0);
        }
    }
}
#endregion
