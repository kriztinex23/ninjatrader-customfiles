#region Using declarations
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Data;
using NinjaTrader.Indicator;
using NinjaTrader.Gui.Chart;
using NinjaTrader.Strategy;
using NadexDBLib;
using TransportModel;
#endregion

// This namespace holds all strategies and is required. Do not change it.
namespace NinjaTrader.Strategy
{
    /// <summary>
    /// Enter the description of your strategy here
    /// </summary>
    [Description("Enter the description of your strategy here")]
    public class MyCustomStrategy : Strategy
    {
        #region Variables
        // Wizard generated variables
        private int myInput0 = 1; // Default setting for MyInput0
        // User defined variables (add any user defined variables below)
        #endregion

        /// <summary>
        /// This method is used to configure the strategy and is called once before any strategy method is called.
        /// </summary>
        protected override void Initialize()
        {
            CalculateOnBarClose = true;
			Print("Stragegy.Initialize  ------- "+DateTime.Now.ToLocalTime());

			 Add(PeriodType.Second, 10);
			if (Historical)Print("Historical");

        }
		

		
        protected override void OnStartUp()
        {
			Print("Stragegy.OnStartUp -------  "+ DateTime.Now.ToLocalTime());
			HourlyPipsForm hpForm = HourlyPipsForm.Instance;
			hpForm.OpenForm();
        }
        /// <summary>
        /// Called on each bar update event (incoming tick)
        /// </summary>
        protected override void OnBarUpdate()
        {
			Print("Strategy.IsOkToTrade : "+ MyCustomIndicator(1).IsOkToTrade   );
        }
		
		protected override void OnFundamentalData(FundamentalDataEventArgs e)
{
		// Print some data to the Output window
		if (e.FundamentalDataType == FundamentalDataType.CurrentYearsEarningsPerShare)
        Print("The current year's EPS is " + e.DoubleValue);
}

        #region Properties
        [Description("")]
        [GridCategory("Parameters")]
        public int MyInput0
        {
            get { return myInput0; }
            set { myInput0 = Math.Max(1, value); }
        }
		

        #endregion
    }
}
